package TestsFor::CloudCAST::Decoder;

use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;
}

sub test_attributes : Tests {
  my $test = shift;

  foreach my $method (qw(
      output_directory pipeline get_element set_element bus
      create_pipeline init_request finish_request end_of_stream
      process_data cancel
    )) {

    can_ok $test->class_name, $method;
  }
}

1;
