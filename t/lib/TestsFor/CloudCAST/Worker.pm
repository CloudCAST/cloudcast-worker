package TestsFor::CloudCAST::Worker;

use Test::Class::Moose;
with 'Test::Class::Moose::Role::AutoUse';

sub test_use : Tests {
  my $test = shift;
  use_ok $test->class_name;
}

sub test_attributes : Tests {
  my $test = shift;

  foreach my $method (qw(
      url inactivity_timeout client segments request_id
      last_updated decoder connection current_transcript
    )) {

    can_ok $test->class_name, $method;
  }
}

1;
