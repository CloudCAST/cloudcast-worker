# NAME

CloudCAST::Worker - A worker class to connect to a CloudCAST server

# SYNOPSIS

    use CloudCAST::Worker;

    my $worker = CloudCAST::Worker->new( $opt );
    $worker->work->recv;

# DESCRIPTION

[CloudCAST::Worker](https://metacpan.org/pod/CloudCAST::Worker) objects open a WebSocket connection to a CloudCAST server
and waits for it to send instructions to process. Most commonly, the first
of these will be an initialisation request from the server, with the details
of the decoder required to process this particular stream of data.

When receiving this message, the worker will initialise a decoder pipeline
by creating a [CloudCAST::Decoder](https://metacpan.org/pod/CloudCAST::Decoder) object, and send a signal to the server
once the pipeline is ready to start receiving data.

The connection to the server is terminated when the decoder has received the
`EOS` signal, and responded in kind; or when an error is encountered during
the decoding process.

When the connection to the client is closed (for the above reasons, or for any
other), the worker will deinitialise (= destroy the decoder) and terminate.

# ATTRIBUTES

- **url**

    The URL of the server to connect to. The server will be reached at this
    endpoint, with the worker's ID as a query parameter.

- **client**

    A reference to the client used by the worker to connect to the server. If not
    otherwise stated, this will default to a [AnyEvent::WebSocket::Client](https://metacpan.org/pod/AnyEvent::WebSocket::Client) object.

- **request\_id**

    A UUID identifying the current connection. This is received from the
    server, and corresponds to the ID number assigned by the server to the
    connection with the client that initiated the request.

- **segments**
- **current\_transcript**

    When using a `onlinegmmdecodefaster` decoder, non-final results are stored in
    this buffer before being sent to the server. This happens when the results are
    marked as final, and then the buffer will reset.

- **last\_updated**

    Stores the last time the worker received a message from the server, in seconds
    from the UNIX epoch.

- **decoder**

    Stores a reference to the linked [CloudCAST::Decoder](https://metacpan.org/pod/CloudCAST::Decoder) object. When this is
    undefined, the worker is said to be _uninitialised_.

- **connection**

    Stores a reference to the connection to the server.

- **inactivity\_timeout**

    The amount of time the worker will wait on an inactive connection before
    terminating. This defaults to 5 seconds. This means that, if processing has
    started, and no message is received in 5 seconds or more, the worker will
    cancel the stream and attempt to close the connection.

- **keep\_alive**

    Under normal operation, the worker listens for a stream, initialises a decoder
    to process an incoming stream, and then destroys the decoder when the stream
    has finished, in order to reset to a raw state, ready for the next stream.

    When this attribute is set to a non-zero value, the worker will keep the
    decoder alive and wait for another stream, which it will then try to decode
    with the same decoder. The amount of time it will wait is equal to the value
    of this attribute in seconds since the last received message.

    For this, it is essential that the CloudCAST server that is forwarding the
    connections to the worker knows the current state of the worker, and follows
    those worker restrictions (so as to not assign to this worker a stream that it
    cannot decode). In order for this to be possible, when establishing a
    connection to the server, the worker reports its current state (which will
    either be _initialised_ or _connected_), and the type of the current decoder,
    if it is initialised.

    This is done under the assumption that an the worker _may_ be able to adapt
    to decode an incoming stream that requires a decoder of the same _type_, but
    certainly not if it needs a different one.

- **post\_process**
- **final\_post\_process**

    These attributes can be set to subroutine references to provide post-processing
    of results before sending them to the client. The `post_process` subroutine
    will be called on every result; while `final_post_process` will only be called
    if the underlying plugin fires a `full-final-result` event. At this point,
    this only happens with the `kaldinnet2onlinedecoder` plugin.

# CONTRIBUTIONS AND BUG REPORTS

Contributions of any kind are most welcome!

The main repository for this distribution is on
[GitLab](https://gitlab.com/CloudCAST/cloudcast-worker), which is where patches
and bug reports are mainly tracked.

If using this channel is not possible, bug reports can also be sent by mail
directly to the developers at the address below, although these will not be as
closely tracked.

# AUTHOR

- José Joaquín Atria <j.atria@sheffield.ac.uk>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by the University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
