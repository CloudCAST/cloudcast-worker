package CloudCAST::Decoder;

our $VERSION = '0';

use Moose;
use MooseX::NonMoose;

with 'CloudCAST::Base';
extends 'AnyEvent::Emitter';

use GStreamer1;
use Types::Path::Tiny qw( Dir );
GStreamer1::init( [$0] );

use Log::Any;
my $log = Log::Any->get_logger;

has options => (
  is => 'rw',
  default => sub { +{} },
  lazy => 1,
);

has [qw( type name )] => ( is => 'rw' );

has output_directory => (
  is => 'rw',
  isa => Dir,
);

has gst_plugin_path => (
  is =>'ro',
  default => $ENV{GST_PLUGIN_PATH},
);

has [qw( pipeline bus )] => (
  is => 'rw',
  default => undef,
);

has _elements => (
  is => 'rw',
  default => sub { +{} },
  lazy => 1,
  traits => ['Hash'],
  handles => {
    get_element => 'get',
    set_element => 'set',
  },
);

sub create_pipeline {
  my ($self) = @_;

  my $options = $self->options;

  $log->debug('Creating GStreamer pipeline');
  $self->pipeline(GStreamer1::Pipeline->new);
  local $ENV{GST_PLUGIN_PATH} = $self->gst_plugin_path;

  my @element_chain;

  foreach (
      [ appsrc        => 'appsrc'        ],
      [ decodebin     => 'decodebin'     ],
      [ audioconvert  => 'audioconvert'  ],
      [ audioresample => 'audioresample' ],
      [ tee           => 'tee'           ],
      [ queue1        => 'queue'         ],
      [ filesink      => 'filesink'      ],
      [ queue2        => 'queue'         ],
      [ asr           => $self->type     ],
      [ fakesink      => 'fakesink'      ],
    ) {

    my ($name, $element) = @{$_};

    my $e = GStreamer1::ElementFactory::make( $element, $name )
      or $self->emit(
        error => sprintf("Failed to create element of type '%s'", $element)
      );

    push @element_chain, $e;

    $self->set_element( $e->get_name, $e );
    $log->debugf('Created %s element', ref $e);
  }

  # Adjust ASR parameters
  foreach (keys %{$options}) {
    $log->debugf('Setting decoder property: %s = %s', $_, $options->{$_});
    $self->get_element('asr')->set_property($_, $options->{$_});
  }

  # Adjust remaining parameters
  $self->get_element('appsrc')->set_stream_type('stream');
  $self->get_element('appsrc')->set_property('is-live', 1);
  $self->get_element('filesink')->set_property('location', '/dev/null');

  $log->debug('Created GStreamer elements');

  # Add elements to pipeline
  foreach (@element_chain) {
    $self->pipeline->add($_);
    $log->debugf('Added %s element to pipeline', ref $_);
  }

  $self->_link_element('appsrc', 'decodebin');

  $self->get_element('decodebin')->signal_connect(
    'pad-added' => sub {
      my ($decoder, $pad, $target) = @_;
      $pad->link($target->get_static_pad('sink'));
    },
    $self->get_element( 'audioconvert' )
  );

  $self->_link_element('audioconvert', 'audioresample');
  $self->_link_element('audioresample', 'tee');
  $self->_link_element('tee', 'queue1');
  $self->_link_element('queue1', 'filesink');
  $self->_link_element('tee', 'queue2');
  $self->_link_element('queue2', 'asr');
  $self->_link_element('asr', 'fakesink');

  # Create bus and connect emitters
  $self->bus($self->pipeline->get_bus);

  $self->bus->add_signal_watch;

  $log->debug('Attaching decoder listeners');

  if ($self->type eq 'onlinegmmdecodefaster') {
    # For the GMM-based decoder
    $self->get_element('asr')->signal_connect(
      'hyp-word' => sub {
        my ($asr, $data, $self) = @_;
        $self->emit( result => { type => 'word', data => $data });
      },
      $self
    );
  }

  elsif ($self->type eq 'kaldinnet2onlinedecoder') {
    # For the NNET2-based decoder
    # TODO Needs testing
    $self->get_element('asr')->signal_connect(
      'partial-result' => sub {
        my ($asr, $data, $self) = @_;
        $self->emit( result => { type => 'partial', data => $data });
      },
      $self
    );

    $self->get_element('asr')->signal_connect(
      'final-result' => sub {
        my ($asr, $data, $self) = @_;
        $self->emit( result => { type => 'final', data => $data });
      },
      $self
    );

    $self->get_element('asr')->signal_connect(
      'full-final-result' => sub {
        my ($asr, $data, $self) = @_;
        $self->emit( result => { type => 'full', data => $data });
      },
      $self
    );
  }

  else {
    $log->errorf(
      q{No listeners implemented for decoder of type '%s'!},
      $self->type);
  }

  $self->bus->signal_connect(
    'message::eos' => sub {
      my $bus = shift;
      my $self = pop;

      $self->finish_request;
      $self->emit( eos => shift );
    },
    $self
  );

  $self->bus->signal_connect(
    'message::error' => sub {
      my $bus = shift;
      my $self = pop;

      $self->emit( error => shift );
      $self->finish_request;
    },
    $self
  );

  $log->debug('Setting pipeline to READY');
  $self->pipeline->set_state('ready');

  $self->emit(ready => undef)
}

sub init_request {
  my ($self, $id, $string_cap) = @_;

  if (defined $string_cap and $string_cap ne '') {
    $self->get_element('appsrc')->set_property(
      caps => GStreamer1::caps_from_string( $string_cap )
    );
  }

  else {
    $self->get_element('appsrc')->set_property( caps => undef );
  }

  if ($self->output_directory) {
    $self->pipeline->set_state('paused');
    $self->get_element('filesink')->set_state('null');
    $self->get_element('filesink')->set_property(
      location =>
        sprintf('%s/%s.raw', $self->output_directory, $self->request_id)
    );
    $self->get_element('filesink')->set_state('playing');
  }

  $self->pipeline->set_state('playing');
  $self->get_element('filesink')->set_state('playing');

  # push empty buffer (to avoid hang on client disconnect)
  my $buffer = GStreamer1::Buffer->new_allocate(undef, 0, undef);
  $self->get_element('appsrc')->push_buffer($buffer);

  $self->emit( start => $string_cap // '<undefined>');
}

sub finish_request {
  my ($self) = @_;

  if ($self->output_directory) {
    $self->get_element('filesink')->set_state('null');
    $self->get_element('filesink')->set_property('location', '/dev/null');
    $self->get_element('filesink')->set_state('playing');
  }
  $self->pipeline->set_state('null');

  $self->emit( stop => undef );
}

sub end_of_stream {
  my ($self) = @_;

  $self->get_element('appsrc')->end_of_stream;
  $self->emit( stop => undef );
}

sub process_data {
  my ($self, $data) = @_;

  my @data = unpack 'c*', $data;

  use bytes;
  my $size = bytes::length($data);

  my $buffer = GStreamer1::Buffer->new_allocate(undef, $size);
  $buffer->fill(0, \@data );
  $self->get_element('appsrc')->push_buffer($buffer);

  $self->emit( pushed => $size );
}

sub cancel {
  my ($self) = @_;

  $self->pipeline->send_event(GStreamer1::Event->new_eos);
  $self->emit( cancel => undef );
}

sub _link_element {
  my $self = shift;
  $self->get_element(shift)->link($self->get_element(shift));
}

1;
