# ABSTRACT: A worker class to connect to a CloudCAST server
package CloudCAST::Worker;

our $VERSION = '0.001';

use Moose;
with 'CloudCAST::Base';

use AnyEvent::WebSocket::Client;
use AnyEvent;
use CloudCAST::Decoder;
use CloudCAST::Codes -all;
use Compress::Zlib qw();
use IPC::Run qw( start finish );
use JSON::MaybeXS qw( decode_json encode_json );
use List::Util qw( any );
use MIME::Base64;
use Path::Tiny qw( path tempfile );
use Try::Tiny;
use Types::Path::Tiny qw( Path );
use Types::Standard qw( InstanceOf Int Str CodeRef );
use Types::URI qw( Uri );
use URI::QueryParam;
use URI;

use Log::Any;
my $log = Log::Any->get_logger;

has [qw(
    request_id last_updated decoder connection
  )] => (

  is => 'rw',
  init_arg => undef,
);

has '+request_id' => ( default => '' );

has current_transcript => (
  traits  => ['String'],
  is      => 'rw',
  isa     => Str,
  default => q{},
  handles => {
    add_to_transcript => 'append',
    clear_transcript  => 'clear',
  },
);

has segments => (
  traits  => ['Counter'],
  is      => 'ro',
  isa     => Int,
  default => 0,
  handles => {
    increment_segments => 'inc',
    decrement_segments => 'dec',
    reset_segments     => 'reset',
  },
);

has url => (
  is => 'ro',
  isa => Uri,
  coerce => 1,
  default => 'localhost',
  lazy => 1,
);

has client => (
  is => 'ro',
  default => sub { AnyEvent::WebSocket::Client->new },
);

has output_directory => (
  is => 'ro',
  isa => Path,
);

has inactivity_timeout => (
  is =>'ro',
  default => 5,
  init_arg => 'timeout',
);

has kaldi_path => (
  is =>'ro',
  default => $ENV{KALDI_ROOT},
  lazy => 1,
);

has gst_plugin_path => (
  is =>'ro',
  default => sub {
    $ENV{GST_PLUGIN_PATH} // path($_[0]->kaldi_path)->child('src/gst-plugin')
  },
);

has keep_alive => (
  is =>'rw',
  isa => Int,
  default => 0,
);

has [qw( post_process final_post_process )] => (
  is => 'rw',
  isa => CodeRef,
  default => sub { sub { return @_ } },
);

has schema => (
  is => 'rw',
  isa => InstanceOf['CloudCAST::Schema'],
  lazy => 1,
  default => sub {
    use CloudCAST::Schema;
    CloudCAST::Schema->connect({ dsn => 'dbi:Pg:' });
  },
);

has decoding_name => (
  is => 'rw',
  isa => Str,
);

sub BUILD {
  my ($self, $args) = @_;
  $self->url->query_param( id => $self->id );
}

sub work {
  my $self = shift;

  my $cv = AnyEvent::condvar;

  $self->client->connect($self->url)->cb( sub {
    my $connection = eval { shift->recv };
    if($@) {
      $log->debugf('Could not connect to %s: %s', $self->url, $@);

      # Send a false value if unable to connect, to break from main loop.
      $cv->send(0);
      return $cv;
    }

    $self->reset_segments;
    $self->connection( $connection );
    if ($self->decoder) {
      $self->state(CLOUDCAST_INITIALIZED);
    }
    else {
      $self->state(CLOUDCAST_CONNECTED);
    }
    $log->info('Opened websocket connection to server');

    {
      my $status = { status => $self->state };

      if ($self->state == CLOUDCAST_INITIALIZED) {
        $status->{$_} = $self->decoder->$_ foreach qw( type name );
      }

      $self->connection->send( encode_json $status );
    }

    # Received a message from the server
    $self->connection->on(each_message => sub {
      my ($connection, $msg) = @_;

      my $state = $self->state;
      if ($msg->is_binary) {
        $log->debugf('%s: Got binary message from server', $self->request_id);

        if ($state < CLOUDCAST_FINISHING) {
          $self->decoder->process_data($msg->body);
        }

        else {
          $log->debugf(
            '%s: Ignoring message, worker already in state %i',
            $self->request_id, $state
          );
        }
      }

      elsif ($msg->is_text) {
        if ($msg->body eq 'EOS') {
          $log->warnf('%s: Got EOS from server', $self->request_id);

          if ($state < CLOUDCAST_FINISHING) {
            $log->warnf('%s: Sent EOS through pipeline', $self->request_id);
            $self->decoder->end_of_stream;
          }

          else {
            $log->infof(
              '%s: Ignoring EOS, worker already in state %i',
              $self->request_id, $state
            );
          }
        }

        else {
          try {
            my $json = decode_json $msg->decoded_body;

            if ($self->request_id eq '' and defined $json->{request_id}) {
              $self->request_id($json->{request_id});
              $log->infof(
                'Established connection with client %s',
                $self->request_id
              );
            }

            if (defined $json->{adaptation_state}) {
              $log->debugf(
                '%s: Got adaptation state from server',
                $self->request_id
              );

              my $adapt =
                $self->_decompress( decode_base64 $msg->{adaptation_state} )
                  or die 'Cannot handle adaptation state';

              $log->infof(
                '%s: Setting adaptation state to user-provided value',
                $self->request_id
              );

              $self->decoder->pipeline->set_adaptation_state($adapt);
            }

            elsif (defined $json->{decoder}) {
              if ($state < CLOUDCAST_INITIALIZED) {
                $log->infof(
                  '%s: %s received init request, initializing %s',
                  $self->request_id, ref $self, __PACKAGE__
                );

                my $decoding = $self->schema->resultset('Decoding')->search({
                  name => $json->{decoder},
                })->first;

                die 'No such decoding: ' . $json->{decoder} unless $decoding;

                # Store decoding name to tell server how are we initialised
                $self->decoding_name( $decoding->uri_name );

                $self->init({
                  type => $decoding->recogniser->type,
                  options => $decoding->merged_parameters,
                });

                # Temporary patch
                $self->decoder->name($json->{decoder});

              }
              else {
                $log->infof(
                  '%s: %s received init request, but worker is already inited',
                  $self->request_id, ref $self
                );
              }

              $self->set_timeout_guard;

              # Sends pipeline::start
              $self->decoder->init_request($self->request_id, $json->{caps});

              $log->infof('%s: Initialised request', $self->request_id);
            }

            else {
              $log->infof("%s: Got a serialised message, but don't know what to do with it", $self->request_id);
              $log->info($msg->decoded_body);
            }
          }
          catch {
            $log->warnf(
              '%s: Could not deserialise message: %s',
              $self->request_id, $_
            );
          };
        }
      }

      else {
        $log->debugf(
          '%s: Got unhandled message with opcode %i',
          $self->request_id, $msg->opcode
        );
      }
    });

    # Connection finished
    $self->connection->on(finish => sub {
      my $connection = @_;

      $log->infof('%s: Worker finishing request', $self->request_id);

      my $state = $self->state;
      if ($state < CLOUDCAST_INITIALIZED) {
        # connection closed after before initialization; nothing to do
      }

      else {
        if ($state == CLOUDCAST_INITIALIZED) {
          # connection closed after linking to decoder, before receiving any data
          $self->decoder->finish_request;
        }

        elsif ($state != CLOUDCAST_FINISHED) {
          # connection closed after linking to decoder, before receiving any data
          $log->infof(
            '%s: Master disconnected before decoder reached EOS?',
            $self->request_id
          );

          $self->decoder->cancel;

          my $counter = 0;
          while ($self->state == CLOUDCAST_ABORTING) {
            $counter += 1;
            if ($counter >= 30) {
              $log->infof(
                '%s: Giving up waiting after %i tries',
                $self->request_id, $counter
              );

              $self->state(CLOUDCAST_FINISHED);
            }

            else {
              $log->infof(
                '%s: Waiting for EOS from decoder',
                $self->request_id
              );

              sleep 1;
            }
          }

          $self->decoder->finish_request;
          $log->infof('%s: Finished waiting for EOS', $self->request_id);
        }
      }

      if ($self->keep_alive) {
        $self->decoder->pipeline->set_state( 'ready' );
        $self->decoder->emit( 'ready' );
        $self->{timeout_guard} = undef;

        $log->infof('%s: Worker will reset in %s seconds',
          $self->request_id, $self->keep_alive);
      }
      else {
        $self->deinit;
      }

      $log->infof('%s: Worker finished request', $self->request_id);

      $cv->send(1);
    });
  });

  return $cv;
}

sub init {
  my ($self, $params) = @_;

  if (ref $self->decoder eq 'CloudCAST::Decoder') {
    $log->warnf('Worker already initialized! (state is %s)', $self->state);
    return;
  }

  $params->{output_directory} //= $self->output_directory
    if $self->output_directory;

  $params->{gst_plugin_path} //= $self->gst_plugin_path;

  $self->decoder(CloudCAST::Decoder->new( $params ));

  $log->debug('Created decoder');

  $self->decoder->on( result => sub {
    my ($decoder, $msg) = @_;

    $self->last_updated(time);
    $self->set_reset_timer;

    return $self->_on_word($msg)       if $msg->{type} eq 'word';
    return $self->_on_partial($msg)    if $msg->{type} eq 'partial';
    return $self->_on_final($msg)      if $msg->{type} eq 'final';
    return $self->_on_full_final($msg) if $msg->{type} eq 'full';
  });

  $self->decoder->on( ready => sub {
    $log->infof(
      '%s: Decoder pipeline is ready. Worker is initialised',
      $self->request_id
    );
    $self->state(CLOUDCAST_INITIALIZED);
  });

  $self->decoder->on( start => sub {
    my ($decoder, $msg) = @_;

    $log->infof(
      '%s: Initialized stream through decoder pipeline. Caps set to %s',
      $self->request_id, $msg
    );
    $self->connection->send( encode_json {
      status => CLOUDCAST_READY,
      name => $self->decoding_name,
    });
    $self->state(CLOUDCAST_READY);
  });

  $self->decoder->on( pushed => sub {
    my ($decoder, $msg) = @_;

    unless ($self->state == CLOUDCAST_STARTED) {
      $self->state(CLOUDCAST_STARTED);
      $log->infof('%s: %s is processing', $self->request_id, ref $self);
    }
    $log->debugf(
      '%s: Pushed %d bytes to decoder pipeline',
      $self->request_id, $msg
    );
  });

  $self->decoder->once( stop => sub {
    $log->infof(
      '%s: Ended stream through decoder pipeline',
      $self->request_id
    );
    $self->state(CLOUDCAST_FINISHING) if $self->state < CLOUDCAST_FINISHING;
  });

  $self->decoder->on( cancel => sub {
    $log->infof(
      '%s: Canceled stream. Sent EOS to decoder pipeline',
      $self->request_id
    );
    $self->state(CLOUDCAST_ABORTING) if $self->state < CLOUDCAST_ABORTING;
  });

  $self->decoder->on( error => sub {
    my ($decoder, $e) = @_;
    my $msg = $e->get_structure->get_value('gerror')->message;
    my $debug = $e->get_structure->get_value('debug');

    $log->errorf( 'Received error from decoder: %s', $msg );
    $log->debugf('Additional debug information: %s', $debug);

    $self->state(CLOUDCAST_FINISHED);
    $self->connection->send( encode_json {
      status => CLOUDCAST_DECODER_ERROR,
      message => $msg,
    });

    # Terminating because endpoint received data it cannot accept
    $self->connection->close(WS_BAD_INPUT, $msg);
  });

  $self->decoder->on( eos => sub {
    my ($decoder, $msg) = @_;

    $log->infof(
      '%s: Received EOS through pipeline. Notifying server',
      $self->request_id
    );

    $self->connection->send( encode_json {
      status => CLOUDCAST_FINISHED,
    });

    # Terminating with normal closure
    $self->connection->close(WS_FINISHED, $msg);
    $self->state(CLOUDCAST_FINISHED);
  });

  $self->decoder->create_pipeline;
}

sub _on_word {
  my ($self, $msg) = @_;

  my $word = $msg->{data};
  my $final = $word && $word eq '<#s>';

  # Not last word, append to partial transcript
  unless ($final) {
    $self->add_to_transcript( q{ } ) if $self->current_transcript;
    $self->add_to_transcript( $word );
  }

  $log->infof( '%s: Sending word result to sever: %s', $self->request_id );

  my ($processed) = $self->post_process->($self->current_transcript);

  $log->infof( '%s: After post-processing: %s', $self->request_id, $processed );

  # Send result
  $self->connection->send( encode_json {
    status => CLOUDCAST_SUCCESS,
    segments => $self->segments,
    result => {
      final => $final,
      hypotheses => [{
        transcript => $processed,
      }],
    },
  });

  # Last word, reset partial transcript
  if ($final) {
    $self->clear_transcript;
    $self->increment_segments;
  }
}

sub _on_partial {
  my ($self, $msg) = @_;

  $log->infof( '%s: Sending partial result to server', $self->request_id );

  my ($processed) = $self->post_process->($msg->{data});

  $log->infof( '%s: After post-processing: %s', $self->request_id, $processed );

  $self->connection->send( encode_json {
    status => CLOUDCAST_SUCCESS,
    result => {
      hypotheses => [{
        transcript => $processed,
      }],
      final => 0,
    },
  });
}

sub _on_final {
  my ($self, $msg) = @_;

  $log->infof( '%s: Sending final result to server', $self->request_id );

  my ($processed) = $self->post_process->($msg->{data});

  $log->infof( '%s: After post-processing: %s', $self->request_id, $processed );

  $self->connection->send( encode_json {
    status => CLOUDCAST_SUCCESS,
    result => {
      final => 1,
      hypotheses => [{
        transcript => $self->post_process->($msg->{data}),
      }],
    },
  });
}

sub _on_full_final {
  my ($self, $msg) = @_;

  $log->infof( '%s: Sending full-final result to server', $self->request_id );

  my $data = decode_json $msg->{data};
  $data->{segments} = $self->segments;
  $data->{status} = CLOUDCAST_SUCCESS if $data->{status} == 0;

  my ($processed) = $self->final_post_process->($data);

  $log->infof( '%s: After post-processing: %s', $self->request_id, $processed );

  $self->connection->send( encode_json $processed );
}

sub deinit {
  my ($self) = @_;
  $log->infof(
    '%s: Destroying decoder. Worker is no longer initialised!',
    $self->request_id
  );
  $self->decoder(undef);
}

sub _decompress {
  my ($self, $string) = @_;

  my $d = Compress::Zlib::inflateInit(-WindowBits => -15);
  my ($inflated, $status) = $d->inflate($string);

  # Check to see if it's actually compressed
  return undef
    if ($status != Compress::Zlib::Z_STREAM_END || length($inflated) <= 1 );

  return $inflated;
}

sub forced_align_grammar {
  my ($self, $input, $word_symbols, $outfile, $options) = @_;
  $options //= {};

  my $fst_text = q{};
  my $i = 0;
  foreach (split / /, $input) {
    my ($from, $to) = ($i++, $i);
    $fst_text .= "$from $to $_\n";
  }

  $self->_fstcompile(
    $fst_text, $outfile,
    {
      acceptor => 1,
      isymbols => $word_symbols,
      %{$options},
    }
  );
}

sub forced_choice_grammar {
  my ($self, $choices, $word_symbols, $outfile, $options) = @_;
  $options //= {};

  $self->_fstcompile(
    _forced_choice_grammar( $choices, $word_symbols ), $outfile,
    {
      isymbols => $word_symbols,
      osymbols => $word_symbols,
      %{$options},
    }
  );
}

# Pass data to fstcompile to generate OpenFST grammars
sub _fstcompile {
  my ($self, $input, $outfile, $opt) = @_;
  $opt //= {};

  $outfile //= tempfile(
    TEMPLATE => 'openfst_grammarXXXXXXXX',
    SUFFIX => '.fst',
  );

  my $openfst = path("$ENV{KALDI_ROOT}/tools/openfst");
  local $ENV{PATH} = $openfst->child('bin');

  die "No binaries for OpenFST at $openfst" unless -e $ENV{PATH};

  my @bool = qw( acceptor keep_isymbols keep_osymbols keep_state_numbering );
  my @cmd = "fstcompile";
  foreach my $k (keys %{$opt}) {
    my $v = $opt->{$k};
    push @cmd, (any { $_ eq $k } @bool) ? "--$k" : "--$k=$v";
  }
  push @cmd, ('-', $outfile);

  my $h = start
    \@cmd,
    '<pipe', \*IN,
    or die "$cmd[0] returned $?";

  print IN $input;
  close IN;

  finish $h;

  return (ref($outfile) eq 'Path::Tiny') ? $outfile : path($outfile);
}

sub _forced_choice_grammar {
  my ($choices, $wordfile) = @_;
  my $epsilon = '<eps>';
  my $backoff = '#0';

  if (scalar @_ > 2 and ref $_[2] eq 'HASH') {
    $epsilon = $_[2]->{epsilon} if defined $_[2]->{epsilon};
    $backoff = $_[2]->{backoff} if defined $_[2]->{backoff};
  }

  my $symbols = map {
    split / /, $_, 2
  } path( $wordfile )->lines_raw({ chomp => 1 });

  my @sym = keys %{$symbols};
  my @chosen = intersect( @{$choices}, @sym );

  my $prob = log scalar @chosen;

  my $fst_text = q{};
  $fst_text .= "0 1 $_ $_ $prob\n" foreach @chosen;

  $fst_text .= "1 $prob\n";
  return $fst_text;
}

sub set_timeout_guard {
  my ($self) = @_;

  $self->last_updated(time);

  if ($self->inactivity_timeout) {
    $self->{timeout_guard} = AnyEvent->timer(
      after => 0,
      interval => 1,
      cb => sub {
        my $state = $self->state;
        if ($state >= CLOUDCAST_INITIALIZED and $state < CLOUDCAST_FINISHED) {
          my $now  = time;
          my $last = $self->last_updated;
          my $latest = $now - $last;

          my $timeout = $self->inactivity_timeout;
          if ($latest > $timeout) {
            $log->warnf(
              '%s: More than %d seconds from last decoder update, cancelling',
              $self->request_id, $timeout
            );
            $self->decoder->cancel;
          }
        }
      },
    );
  }

  $log->infof('%s: Timeout guard is%s active',
    $self->request_id, ($self->{timeout_guard} ? q{} : ' NOT') );
}

sub set_reset_timer {
  my ($self) = @_;

  return unless $self->keep_alive;

  $self->{reset_timeout} = AnyEvent->timer(
    after => $self->keep_alive,
    cb => sub {
      $log->infof('%s: Worker resetting!', $self->request_id);

      $self->decoder->cancel
        if  $self->state >= CLOUDCAST_STARTED
        and $self->state <= CLOUDCAST_FINISHED;

      $self->state( CLOUDCAST_CONNECTED );
      $self->deinit;
    },
  );
}

1;

__END__

=encoding utf8

=head1 NAME

CloudCAST::Worker - A worker class to connect to a CloudCAST server

=head1 SYNOPSIS

  use CloudCAST::Worker;

  my $worker = CloudCAST::Worker->new( $opt );
  $worker->work->recv;

=head1 DESCRIPTION

L<CloudCAST::Worker> objects open a WebSocket connection to a CloudCAST server
and waits for it to send instructions to process. Most commonly, the first
of these will be an initialisation request from the server, with the details
of the decoder required to process this particular stream of data.

When receiving this message, the worker will initialise a decoder pipeline
by creating a L<CloudCAST::Decoder> object, and send a signal to the server
once the pipeline is ready to start receiving data.

The connection to the server is terminated when the decoder has received the
C<EOS> signal, and responded in kind; or when an error is encountered during
the decoding process.

When the connection to the client is closed (for the above reasons, or for any
other), the worker will deinitialise (= destroy the decoder) and terminate.

=head1 ATTRIBUTES

=over 4

=item B<url>

The URL of the server to connect to. The server will be reached at this
endpoint, with the worker's ID as a query parameter.

=item B<client>

A reference to the client used by the worker to connect to the server. If not
otherwise stated, this will default to a L<AnyEvent::WebSocket::Client> object.

=item B<request_id>

A UUID identifying the current connection. This is received from the
server, and corresponds to the ID number assigned by the server to the
connection with the client that initiated the request.

=item B<segments>

=item B<current_transcript>

When using a C<onlinegmmdecodefaster> decoder, non-final results are stored in
this buffer before being sent to the server. This happens when the results are
marked as final, and then the buffer will reset.

=item B<last_updated>

Stores the last time the worker received a message from the server, in seconds
from the UNIX epoch.

=item B<decoder>

Stores a reference to the linked L<CloudCAST::Decoder> object. When this is
undefined, the worker is said to be I<uninitialised>.

=item B<connection>

Stores a reference to the connection to the server.

=item B<inactivity_timeout>

The amount of time the worker will wait on an inactive connection before
terminating. This defaults to 5 seconds. This means that, if processing has
started, and no message is received in 5 seconds or more, the worker will
cancel the stream and attempt to close the connection.

=item B<keep_alive>

Under normal operation, the worker listens for a stream, initialises a decoder
to process an incoming stream, and then destroys the decoder when the stream
has finished, in order to reset to a raw state, ready for the next stream.

When this attribute is set to a non-zero value, the worker will keep the
decoder alive and wait for another stream, which it will then try to decode
with the same decoder. The amount of time it will wait is equal to the value
of this attribute in seconds since the last received message.

For this, it is essential that the CloudCAST server that is forwarding the
connections to the worker knows the current state of the worker, and follows
those worker restrictions (so as to not assign to this worker a stream that it
cannot decode). In order for this to be possible, when establishing a
connection to the server, the worker reports its current state (which will
either be I<initialised> or I<connected>), and the type of the current decoder,
if it is initialised.

This is done under the assumption that an the worker I<may> be able to adapt
to decode an incoming stream that requires a decoder of the same I<type>, but
certainly not if it needs a different one.

=item B<post_process>

=item B<final_post_process>

These attributes can be set to subroutine references to provide post-processing
of results before sending them to the client. The C<post_process> subroutine
will be called on every result; while C<final_post_process> will only be called
if the underlying plugin fires a C<full-final-result> event. At this point,
this only happens with the C<kaldinnet2onlinedecoder> plugin.

=back

=head1 CONTRIBUTIONS AND BUG REPORTS

Contributions of any kind are most welcome!

The main repository for this distribution is on
L<GitLab|https://gitlab.com/CloudCAST/cloudcast-worker>, which is where patches
and bug reports are mainly tracked.

If using this channel is not possible, bug reports can also be sent by mail
directly to the developers at the address below, although these will not be as
closely tracked.

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <j.atria@sheffield.ac.uk>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by the University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
